﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnce : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private float delay;

    private void Start()
    {
        StartCoroutine(Rotate360());
    }

    IEnumerator Rotate360()
    {
        yield return new WaitForSeconds(delay);

        float angle = 0f;
        while (angle < 360f)
        {
            angle += Time.deltaTime * 360f / duration;
            Debug.Log(angle);
            gameObject.transform.localRotation = Quaternion.Euler(0, Mathf.Min(angle, 360f), 0);
            yield return 0;
        }
    }
}
