﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsInit : MonoBehaviour
{
    [SerializeField] private SettingsScriptableObject _defaultSettings;
    [SerializeField] private SettingsScriptableObject _settings;
    // Start is called before the first frame update
    void Start()
    {
        _settings.SetVolume(_defaultSettings.Volume);
        _settings.SetMouseSens(_defaultSettings.MouseSens);
        _settings.SetHrtfFile(_defaultSettings.HrtfFile);
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
