﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game Settings")]
public class SettingsScriptableObject : ScriptableObject
{
    [SerializeField] private float volume;
    [SerializeField] private float mouseSens;
    [SerializeField] private string hrtfFile;
    
    public float Volume { get { return volume; } }
    public float MouseSens { get { return mouseSens; } }
    public string HrtfFile { get { return hrtfFile; } }
    
    public void SetVolume (float vol) { volume = vol; }
    public void SetMouseSens (float sens) { mouseSens = sens; } 
    public void SetHrtfFile (string hrtf) { hrtfFile = hrtf; }
}
