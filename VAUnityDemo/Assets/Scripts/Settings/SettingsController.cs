﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VAUnity;

public class SettingsController : MonoBehaviour
{
    [SerializeField] private SettingsScriptableObject settings;
    void Start()
    {
        foreach (var look in FindObjectsOfType<PlayerLook>())
        {
            look.setMouseSensitivity(settings.MouseSens);
        }
        foreach (var source in FindObjectsOfType<VAUSoundSourceGeneric>())
        {
            //source.soundpower (settings.MouseSens);
        }
        /*foreach (var look in FindObjectsOfType<PlayerLook>())
        {
            look.setMouseSensitivity(settings.MouseSens);
        }*/
    }
}
