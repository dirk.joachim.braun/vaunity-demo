﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField]  private Slider _volumeSlider;
    [SerializeField]  private Slider _mouseSensitivitySlider;
    [SerializeField]  private TMP_Text _hrtfFile;

    [SerializeField] private SettingsScriptableObject _settings;
    
    void OnEnable()
    {
        _volumeSlider.value = _settings.Volume;
        _mouseSensitivitySlider.value = _settings.MouseSens;
        _hrtfFile.text = _settings.HrtfFile;
    }
}
