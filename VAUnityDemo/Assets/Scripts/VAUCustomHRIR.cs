﻿using System;
using UnityEngine;

namespace VAUnity
{
    public class VAUCustomHRIR : VAUDirectivity
    {

        // The default HRIR create a HRIR database using the default macro $(DefaultHRIR), which is usually defined in the server configuration"
        VAUCustomHRIR()
        {
            //FilePath = string.IsNullOrEmpty(Settings.hrtf) ? "$(DefaultHRIR)" : Settings.hrtf;
            //FilePath = "ASD";
            //Name = "Customizable head-related transfer function";
            //Debug.Log("To Load: " + FilePath);
        }

        protected override void Awake()
        {
            //FilePath = string.IsNullOrEmpty(Settings.hrtfFile) ? "$(DefaultHRIR)" : Settings.hrtfFile;
            base.Awake();
        }
    }   
}
