﻿using System;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private float distance = 3f;
    [SerializeField] private LayerMask mask;
    
    private PlayerUI _playerUI;
    private Camera _cam;
    private InputManager _inputManager;

    // Start is called before the first frame update
    void Start()
    {
        _cam = GetComponent<PlayerLook>().cam;
        _playerUI = GetComponent<PlayerUI>();
        _inputManager = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        _playerUI.UpdateText(String.Empty);
        
        var transform1 = _cam.transform;
        Ray ray = new Ray(transform1.position, transform1.forward);
        Debug.DrawRay(ray.origin, ray.direction * distance);
        
        if (Physics.Raycast(ray, out var hitInfo, distance, mask))
        {
            if (hitInfo.collider.GetComponent<Interactable>() != null)
            {
                Interactable interactable = hitInfo.collider.GetComponent<Interactable>();
                _playerUI.UpdateText(interactable.promptMessage);
                if (_inputManager.Walk.Interact.triggered)
                {
                    interactable.BaseInteract();
                }
            }
        }
    }
}
