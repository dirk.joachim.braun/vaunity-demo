﻿using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public float walkingSpeed = 5f;
    public float sprintSpeed = 8f;
    public float crouchSpeed = 3f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    
    private CharacterController _controller;
    private Vector3 _playerVelocity;

    private bool _isGrounded;

    private bool _crouching;
    private bool _lerpCrouch;
    private float _crouchTimer;
    
    private bool _sprinting;
    private float _speed;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _speed = walkingSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = _controller.isGrounded;

        if (_lerpCrouch)
        {
            _crouchTimer += Time.deltaTime;
            float p = _crouchTimer / 1;
            p *= p;
            _controller.height = Mathf.Lerp(_controller.height, _crouching ? 1 : 2, p);
            // transform.localScale = new Vector3(1,_controller.height / 2f,1);

            if (p > 1)
            {
                _lerpCrouch = false;
                _crouchTimer = 0f;
            }
        }
    }

    // Receive inputs from InputManager.cs and apply them to the Character controller
    public void ProcessMove(Vector2 input)
    {
        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        _controller.Move(_speed * Time.deltaTime * transform.TransformDirection(moveDirection));

        _playerVelocity.y += gravity * Time.deltaTime;
        if (_isGrounded && _playerVelocity.y < 0)
            _playerVelocity.y = -2f;
        _controller.Move(Time.deltaTime * _playerVelocity);
    }

    public void Jump()
    {
        if (_isGrounded)
        {
            _playerVelocity.y = Mathf.Sqrt(jumpHeight * -3f * gravity);
        }
    }

    public void Crouch()
    {
        _crouching = !_crouching;
        _crouchTimer = 0f;
        _lerpCrouch = true;
        _speed = _crouching ? crouchSpeed : walkingSpeed;
    }

    public void Sprint()
    {
        _sprinting = !_sprinting;
        _speed = _sprinting ? sprintSpeed : walkingSpeed;
    }
}
