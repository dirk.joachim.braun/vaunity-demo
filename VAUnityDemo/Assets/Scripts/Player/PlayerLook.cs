﻿using System;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Camera cam;
    

    private float _xRotation;
    [SerializeField] private float _xSensitivity=5f;
    private float _ySensitivity=5f  ;

    public void setMouseSensitivity(float sens)
    {
        _xSensitivity = sens;
        _ySensitivity = sens;
    }

    public void ProcessLook(Vector2 input)
    {
        var mouseX = input.x;
        var mouseY = input.y;
        
        // calculate camera rotation for looking up and down
        _xRotation -= (mouseY * Time.deltaTime) * _ySensitivity;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);
        
        // apply this to the camera transform
        cam.transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
        
        // rotate Player to look left and right
        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime * _xSensitivity));
    }
}
