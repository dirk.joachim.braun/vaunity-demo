﻿using UnityEngine;
// using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private PlayerInput _playerInput;
    public PlayerInput.WalkActions Walk; // was private (but used in Interact??)

    private PlayerMotor _motor;
    private PlayerLook _look;

    // Start is called before the first frame update
    void Awake()
    {
        _playerInput = new PlayerInput();
        Walk = _playerInput.Walk;
        
        _motor = GetComponent<PlayerMotor>();
        _look = GetComponent<PlayerLook>();
        
        Walk.Jump.performed += ctx => _motor.Jump();
        Walk.Crouch.performed += ctx => _motor.Crouch();
        Walk.Sprint.performed += ctx => _motor.Sprint(); 
        
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // tell the PlayerMotor to move using the value from the movement action.
        _motor.ProcessMove(Walk.Movement.ReadValue<Vector2>());
    }

    private void LateUpdate()
    {
        
        _look.ProcessLook((Walk.Look.ReadValue<Vector2>()));
    }

    private void OnEnable()
    {
        Walk.Enable();
    }

    private void OnDisable()
    {
        Walk.Disable();
    }
}
