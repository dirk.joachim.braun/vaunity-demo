﻿using System;
using UnityEngine;
using TMPro;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI promptText;
    [SerializeField] private GameObject crossHair;
    
    private static readonly int IsActive = Animator.StringToHash("isActive");
    private Animator _animator;

    private void Start()
    {
        _animator = crossHair.GetComponent<Animator>();
    }

    public void UpdateText(string promptMessage)
    {
        promptText.text = promptMessage;
        _animator.SetBool(IsActive, promptMessage != String.Empty);
    }
}
