﻿using TMPro;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textMesh;
    
    private float[] _fpsHistory;
    private int _fpsIterator;
    
    // Start is called before the first frame update
    void Start()
    {
        _fpsIterator = 0;
        _fpsHistory = new float[64];
        //foreach (float value in _fpsHistory)
        //    value = Mathf.Round(1.0f / Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        // Update FPS Array
        _fpsIterator = (_fpsIterator + 1) < _fpsHistory.Length ? (_fpsIterator + 1) : 0;
        _fpsHistory[_fpsIterator] = 1.0f / Time.deltaTime;

        // Calculate Average
        float average = 0;
        foreach (float value in _fpsHistory)
            average += value;
        average /= _fpsHistory.Length;
        
        // Update Text Display
        textMesh.text = Mathf.Round(average).ToString(new System.Globalization.CultureInfo("en-US")) + " fps";
    }
}
