﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private bool isPaused;
    [SerializeField] private InputManager inputManager;

    private PlayerInput _playerInput;
    private InputAction _menu;

    private InputActionMap _ui;
    
    private void Awake()
    {
        _playerInput = new PlayerInput();
    }

    private void OnEnable()
    {
        _menu = _playerInput.Menu.Escape;
        _menu.Enable();
        _menu.performed += Pause;

        _ui = _playerInput.UI;
    }

    private void OnDisable()
    {
        _menu.Disable();    
    }

    void Pause(InputAction.CallbackContext ctx)
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            ActivateMenu();
        }
        else
        {
            DeactivateMenu();
        }
    }

    void ActivateMenu()
    {
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
        
        _ui.Enable();
        _playerInput.Walk.Disable();
        
        Cursor.lockState = CursorLockMode.None;
    }

    public void DeactivateMenu()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        
        _ui.Disable();
        _playerInput.Walk.Enable();
            
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        isPaused = false;
        SceneManager.LoadScene(0);
    }
    
    public void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
