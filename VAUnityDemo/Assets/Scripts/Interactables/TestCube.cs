﻿using UnityEngine;

public class TestCube : Interactable
{
    [SerializeField] private GameObject cube; // obsolete; this is itself

    private bool _isActive;
    
    // Cache these two for performance (Rider suggestion)
    private Animator _animator;
    private static readonly int IsActiveHash = Animator.StringToHash("isActive");

    private void Start()
    {
        _animator = cube.GetComponent<Animator>();
    }

    protected override void Interact()
    {
        // Debug.Log("Interacted with ." + gameObject.name);
        _isActive = !_isActive;
        
        // changed from 
        // cube.GetComponent<Animator>().SetBool("isActive", _isActive);
        // due to performance
        _animator.SetBool(IsActiveHash, _isActive);
    }
}
