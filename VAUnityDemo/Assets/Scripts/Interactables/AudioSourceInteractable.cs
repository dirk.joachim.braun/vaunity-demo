﻿using UnityEngine;
using VAUnity;

public class AudioSourceInteractable : Interactable
{
    [SerializeField] private Progressbar _progressbar;
    [SerializeField] private Destuctible _destuctible;
    private VAUSoundSourceGeneric _soundSource;
    private void Start()
    {
        _soundSource = GetComponent<VAUSoundSourceGeneric>();
    }

    protected override void Interact()
    {
        // Disabled due to rare calls (only gets call whe Player clicks on item)
        // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
        if(_soundSource.GetPlaybackState())
            _soundSource.Stop();
        else
            _soundSource.Play();
        
        _progressbar.IncreaseProgress();
        
        Camera camera = (Camera) FindObjectOfType(typeof(Camera));
        BirdController birdController = GetComponent<BirdController>();
        if (camera && birdController)
        {
            birdController.FleeFrom(camera.transform.position);
        }

        // Start Cage Destruction Animation
        if (_destuctible)
        {
            _destuctible.destroy();
        }
        
        // Delete Object and all Children in 5 seconds
        foreach (Transform g in transform.GetComponentsInChildren<Transform>())
        {
            Destroy(g.gameObject, 5);
        }
    }
}
