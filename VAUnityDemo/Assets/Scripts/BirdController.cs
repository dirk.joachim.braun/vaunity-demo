using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {

	Animator _anim;

	bool _idle = true;
	bool _onGround = true;
	
	SphereCollider _solidCollider;
	float _agitationLevel = .5f;

	//hash variables for the animation states and animation properties
	int _idleAnimationHash;
	
	int _singAnimationHash;
	int _ruffleAnimationHash;
	int _preenAnimationHash;
	int _peckAnimationHash;
	
	int _flyAnimationHash;
	int _flyingBoolHash;
	
	int _peckBoolHash;
	int _ruffleBoolHash;
	int _preenBoolHash;
	int _singTriggerHash;
	
	int _flyingDirectionHash;
	
	private Rigidbody _rigidbodyComponent;

	private void Start()
	{
		_rigidbodyComponent = GetComponent<Rigidbody>();
	}

	void OnEnable () {
		_solidCollider = gameObject.GetComponent<SphereCollider>();
		_anim = gameObject.GetComponent<Animator>();

		_idleAnimationHash = Animator.StringToHash("Base Layer.Idle");
		_flyAnimationHash = Animator.StringToHash ("Base Layer.fly");
		_flyingBoolHash = Animator.StringToHash("flying");
		_peckBoolHash = Animator.StringToHash("peck");
		_ruffleBoolHash = Animator.StringToHash("ruffle");
		_preenBoolHash = Animator.StringToHash("preen");
		_singTriggerHash = Animator.StringToHash ("sing");
		_flyingDirectionHash = Animator.StringToHash("flyingDirectionX");
		_anim.SetFloat ("IdleAgitated",_agitationLevel);
	}

	IEnumerator FlyOff(Vector3 target)
	{
		_onGround = false;
		
		_rigidbodyComponent.isKinematic = false;
		_rigidbodyComponent.velocity = Vector3.zero;
		_rigidbodyComponent.drag = 0.5f;

		_anim.applyRootMotion = false;
		
		_anim.SetBool(_flyingBoolHash, true);
		// Wait for the flying animation
		while(_anim.GetCurrentAnimatorStateInfo(0).fullPathHash != _flyAnimationHash)
			yield return 0;
		
		//birds fly up and away from their perch for 1 second before orienting to the target
		_rigidbodyComponent.AddForce(0.0f * transform.forward + 20.0f * transform.up);
		//yield return new WaitForSeconds(0.5f);
		
		Quaternion finalRot = Quaternion.identity, startRot = transform.rotation;
		var distanceToTarget = Vector3.Distance(transform.position, target);
		var t = 0f;
		
		// TODO: probably implement here the case, if the target is too steep above the bird
		
		//rotate the bird toward the target over time
		while(transform.rotation != finalRot || distanceToTarget >= 1.5f){
			distanceToTarget = Vector3.Distance (transform.position,target);
			var direction = (target-transform.position).normalized;
			if(direction==Vector3.zero){
				direction = new Vector3(0.0001f,0.00001f,0.00001f);
			}

			finalRot = Quaternion.LookRotation(direction);
			transform.rotation = Quaternion.Slerp (startRot,finalRot,5f*t);
			_anim.SetFloat (_flyingDirectionHash,FindBankingAngle(transform.forward,direction));
			t += Time.deltaTime;
			_rigidbodyComponent.AddForce(transform.forward * (200.0f * Time.deltaTime));
			yield return 0;
		}
		
		// Fly towards the target
		float flyingForce = 100.0f;
		while (true)
		{
			var direction = (target - transform.position).normalized;
			finalRot = Quaternion.LookRotation(direction);
			_anim.SetFloat (_flyingDirectionHash,FindBankingAngle(transform.forward,direction)); // TODO: try to remove Banking angle
			transform.rotation = finalRot;
			_rigidbodyComponent.AddForce(transform.forward * (flyingForce * Time.deltaTime));
			distanceToTarget = Vector3.Distance (transform.position,target);
			if (distanceToTarget <= 5.0f)
				_rigidbodyComponent.drag = 1.0f;
			if (distanceToTarget <= 1.5f)
			{
				_solidCollider.enabled = false;
				_rigidbodyComponent.drag = 2.0f;
			}
			if(distanceToTarget < 0.5f)
				break;
			yield return 0;
			
			Debug.DrawLine(transform.position, target);
		}
		
		
		yield return null;
	}

	//Sets a variable between -1 and 1 to control the left and right banking animation
	float FindBankingAngle(Vector3 birdForward, Vector3 dirToTarget){
		Vector3 cr = Vector3.Cross (birdForward,dirToTarget);
		float ang = Vector3.Dot (cr,Vector3.up);
		return ang;
	}
	
	void OnGroundBehaviors(){
		_idle = _anim.GetCurrentAnimatorStateInfo(0).fullPathHash == _idleAnimationHash;
		if(!GetComponent<Rigidbody>().isKinematic){
			GetComponent<Rigidbody>().isKinematic = true;
		}
		if(_idle){
			//the bird is in the idle animation, lets randomly choose a behavior every 3 seconds
			if (Random.value < Time.deltaTime*.33){
				//bird will display a behavior
				// TODO: geht das nicht besser?
				float rand = Random.value;
				if (rand < .3f){
					_anim.SetTrigger(_singTriggerHash);
				}else if (rand < .5f){
					_anim.SetTrigger(_peckBoolHash);
				}else if (rand < .8f){
					_anim.SetTrigger(_preenBoolHash);	
				}else if (rand < 1.0f){
					_anim.SetTrigger(_ruffleBoolHash);	
				}else{
					_anim.SetTrigger(_singTriggerHash);	
				}
				//lets alter the agitation level of the brid so it uses a different mix of idle animation next time
				_anim.SetFloat ("IdleAgitated",Random.value);
			}
		}
	}

	public void FleeFrom(Vector3 fleeFrom)
	{
		var position = transform.position;
		var target = position - fleeFrom; // get Direction from fleeFrom to this target
		target *= 10;
		target.y = 10;
		target = Quaternion.Euler(0, Random.Range(-30f, 30f), 0) * target; // rotate around small random angle
		
		target += position;	// transform direction to target position
		
		StartCoroutine("FlyOff", target);
	}

	void Update () {
		if(_onGround){
			OnGroundBehaviors();	
		}
	}
	
	public void PlaySong(){}
}
