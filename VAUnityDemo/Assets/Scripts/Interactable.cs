﻿using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public string promptMessage = "Press to interact";

    // The Player will call this function
    public void BaseInteract()
    {
        Interact();
    }

    protected virtual void Interact()
    {
        // to be filled by subclasses
    }
}
