﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class RenderImage : MonoBehaviour
{
    [SerializeField] private string fileName;
    [SerializeField] private RenderTexture texture;

    private bool _rendered = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!_rendered)
        {
            RenderFrame();
        }
    }

    void RenderFrame()
    {
        
        Texture2D texture2D = new Texture2D(texture.width, texture.height);
        RenderTexture.active = texture;
        texture2D.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
        texture2D.Apply();

        string Path = "Assets/" + fileName + ".png";
        byte[] bytes = texture2D.EncodeToPNG();

        File.WriteAllBytes(Path, bytes);
    }
}
