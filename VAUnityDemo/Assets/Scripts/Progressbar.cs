﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Progressbar : MonoBehaviour
{
    [SerializeField] private int updateRate = 1;
    [SerializeField] private int progess = 0;
    [SerializeField] private int maxProgrss = 5;
    [SerializeField] private Sprite fullBird;
    [SerializeField] private Sprite emptyBird;
    [SerializeField] private Image[] birds;

    private void Start()
    {
        InvokeRepeating("UpdateProgress", 0, updateRate);
    }

    public void IncreaseProgress()
    {
        progess = progess < maxProgrss ? progess + 1 : maxProgrss;
        UpdateProgress();
    }

    private void UpdateProgress()
    {
        for (int i = 0; i < birds.Length; i++)
        {
            if (i < progess)
                birds[i].sprite = fullBird;
            else
                birds[i].sprite = emptyBird;
            
            if (i < maxProgrss)
                birds[i].enabled = true;
            else
                birds[i].enabled = false;
        }
    }
}
