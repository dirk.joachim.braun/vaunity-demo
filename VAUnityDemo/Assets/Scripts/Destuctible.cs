﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destuctible : MonoBehaviour
{	
	public void destroy()
	{
		gameObject.GetComponent<CapsuleCollider>().enabled = false;
		foreach (Transform g in transform.GetComponentsInChildren<Transform>())
        {
			if(g.GetComponent<MeshCollider>() != null && g.GetComponent<Rigidbody>() != null)
			{
				g.GetComponent<MeshCollider>().enabled = true;
				g.GetComponent<Rigidbody>().useGravity = true;
			}
        }
		// StartCoroutine(FallOff());
		Invoke("FallOffInvoke", 1); // execute function in one second
	}
	
	void FallOffInvoke()
	{
		foreach (Transform g in transform.GetComponentsInChildren<Transform>())
        {
			if(g.GetComponent<MeshCollider>() != null && g.GetComponent<Rigidbody>() != null)
			{
				g.GetComponent<MeshCollider>().enabled = false;
			}
			Destroy(g.gameObject, 3);
        }
	}
	
	IEnumerator FallOff()
	{
		yield return new WaitForSeconds(1);
		
		foreach (Transform g in transform.GetComponentsInChildren<Transform>())
        {
			if(g.GetComponent<MeshCollider>() != null && g.GetComponent<Rigidbody>() != null)
			{
				g.GetComponent<MeshCollider>().enabled = false;
			}
			Destroy(g.gameObject, 3);
        }
	}
	
}
