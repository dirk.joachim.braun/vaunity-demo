﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetQuality : MonoBehaviour
{
    [SerializeField] private Dropdown quality;

    public void SetQualityDropdown(int index)
    {
        QualitySettings.SetQualityLevel(index, false);
    }
}
