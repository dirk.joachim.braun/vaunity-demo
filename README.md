# Binaural Songbirds
This project showcases the integration of the VA framework in Unity using the [VAUnity package](https://git.rwth-aachen.de/ita/vaunity_package).
It was created as part of a seminar at the [IHTA](https://www.akustik.rwth-aachen.de) at RWTH University.

Take a walk through a beautiful forest, listen to singing birds and localize them acoustically.

![Screenshot](assets/screenshot.jpg)

## Table of Contents
- [Getting Started](#getting-started)
    - [Cloning the Project](#cloning-the-project)
    - [Requirements](#requirements)
- [Repository Structure](#repository-structure)
- [Usage](#usage)
- [Features](#features)
- [Bird songs](#bird-songs)
- [3<sup>rd</sup> Party Assets and Plugins](#3rd-party-assets-and-plugins)
- [Troubleshooting](#troubleshooting)

---

# Getting Started

## Cloning the Project
It is crucial to recursively clone the repository as the VAUnity package is included as a submodule.
The most convenient approach is to clone the repository with `git clone --recursive` (tested with Git version 2.41).
It may be necessary to switch the branch of the submodules; for additional information, please refer to the [Troubleshooting](#troubleshooting) section.

## Requirements
This project was created using the Unity Editor version *2019.4.40f1*.
Although it has been briefly tested with other versions and may still function, we recommend using the version mentioned above.
To manage multiple editor versions (and different projects), Unity offers the [Unity Hub](https://unity.com/download).

The required plugins will be imported automatically upon opening the project in Unity and do not need to be downloaded explicitly.

# Usage
Open the project from the Unity Hub using the correct Unity Editor version.
The first opening after cloning will take a while as Unity compiles all scripts and imports all assets to the library.
Subsequent starts will be much faster.

# Features
The game features a small forest-themed scene that can be explored using common game controls (WASD-keys or arrow keys for movement and mouse for looking around).
Six different birds are hidden in the grass, trapped in small cages.
The in-game objective is to locate the birds by listening for their calls and then releasing them by clicking on the cage.
Two additional sound sources are given: a broken radio that can be turned on or off, and a campfire.
These enhance the atmosphere and give an easier target to locate compared to the narrow-band bird songs.

The following video shows how this functions.
At first, you can see the VA framework starts up in a console window.
A quick 360° pan follows to give an impression of the schene.
Then, one of the birds is released.
The player walks up to the closest cage and presses release ("Meise freilassen" in german).
Using headphones is recommended to experience the spacial audio created with the binaural synthesis.

![Demo Video](assets/demo.mp4)

# Bird songs

The bird songs used in this project were recored in Aachen, Germany and post-processed using Audition from the Adobe CC. The include a blackbird (Turdus merula), a common chaffinch (Fringilla coelebs), a great tit (Parus major), an european robin (Erithacus rubecula), an eurasian wren (Troglodytes troglodytes) and a common chiffchaff (Phylloscopus collybita).

# 3<sup>rd</sup> Party Assets and Plugins 
This project uses various free assets from the asset store.
All of them are licensed under the [Standard Unity Asset Store EULA](https://unity.com/legal/as-terms).
The following list acknowledges the corresponding creators:
- [Fantasy Forest Environment](https://assetstore.unity.com/packages/3d/environments/fantasy/fantasy-forest-environment-free-demo-35361) - Free Demo *by TriForge Assets* for the tree and grass assets
- [Living Birds](https://assetstore.unity.com/packages/3d/characters/animals/birds/living-birds-15649) *by Dinopunch* for the bird models and animations
- [Terrain Sample Asset Pack](https://assetstore.unity.com/packages/3d/environments/landscapes/terrain-sample-asset-pack-145808) *by Unity Technologies* for the elevation stamps and ground textures
- [AllSky Free - 10 Sky / Skybox Set](https://assetstore.unity.com/packages/2d/textures-materials/sky/allsky-free-10-sky-skybox-set-146014) *by rpgwhitelock* for the skybox
- [Simple UI & icons](https://assetstore.unity.com/packages/2d/gui/icons/simple-ui-icons-147101) *by madder* for the UI elements used to build the menu and overlay
- [Radio](https://assetstore.unity.com/packages/3d/props/radio-230712) *by AK STUDIO ART* for the radio model

The following packages have been used:
- _Terrain Tools_ for terrain creation
- _Textmesh Pro_ as a better text renderer
- _Input System_ to replace the old Unity inputs
- _Post Processing_ for some visual effects

# Troubleshooting
The project has some known issues which will be addressed here.

Most notably, at the moment there is a bug in the VAUnity package, which hinders Unity from building projects.
As a result, this repo does not contain any ready-to-use assets.
This will hopefully be fixed in the future.
Currently, the only way to use the demo is by installing the Unity Editor and launching the project through it.

Another issue occurs on certain hardware.
We found a slight correlation between this problem and the absence of a dedicated graphics card, but it is not the cause of it.
This causes the project to crash short after starting VA.
When VAUnity starts the VA server, it does not wait sufficient time before attempting to connect.
This results in a few error messages, particularly the `Could not connect to VA server on localhost using port 12340` error.
A fix for this bug has already been implemented in the VAUnity repository in the development branch.
Unfortunately, it is not possible to switch a submodule's submodule branch.
If this error occurs, one could manually navigate to the VAUnity submodule folder 
`VAUnityDemo\Assets\Plugins\vaunity_package\Runtime\VAUnity` or `VAUnityMinimal\Assets\Plugins\vaunity_package\Runtime\VAUnity`
and checkout the development branch with `git checkout develop` and try to start the demo again.
To test this, a secondary Unity project has been implemented, which was part of this repository until version 0.4 (see tag `v0.4`).
It has since been removed because it required a duplicate copy of the VAUnity package and therefore used a lot of space when cloning.
